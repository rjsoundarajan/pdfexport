import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpserviceService {

  constructor() { }

  getTestData(){
    return [
      {name:"soundar",degree:'BCA',totalMark:100},
      {name:"rajan",degree:'MSC.CS',totalMark:90},
      {name:"selvan",degree:'MSC.CS',totalMark:48},
      {name:"selvaraj",degree:'BSC.CS',totalMark:60},
      {name:"tamil",degree:'BCOM.CA',totalMark:75}
    ]
  }
}
