import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';

import { UserRoutingModule } from './user-routing.module';
import { HttpserviceService } from '../service/httpservice.service';
import { UserComponent } from './user.component';
import { UserlistComponent } from './userlist/userlist.component';



@NgModule({
  declarations: [UserComponent,UserlistComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    PDFExportModule
  ],
  providers:[HttpserviceService]
})
export class UserModule { }
