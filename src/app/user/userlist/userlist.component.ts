import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpserviceService } from 'src/app/service/httpservice.service';
// import * as jsPDF from 'jspdf';


@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserlistComponent implements OnInit {

  constructor(private http : HttpserviceService) { }
  userLists:Array<any>=[]

  
  ngOnInit(): void {
   this.userLists= this.http.getTestData()
    console.log('get Data',this.userLists)
  }




}
